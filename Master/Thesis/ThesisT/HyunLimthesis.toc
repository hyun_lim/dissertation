\contentsline {chapter}{Acknowledgments}{iii}{chapter*.1}
\contentsline {chapter}{Abstract}{iv}{chapter*.2}
\contentsline {chapter}{List of Tables}{vi}{section*.4}
\contentsline {chapter}{List of Figures}{vii}{section*.5}
\contentsline {part}{Chapters}{1}{section*.6}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Review of Relativistic Quantum Mechanics}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Review of the Finite Element Method}{1}{section.1.2}
\contentsline {chapter}{\numberline {2}Numerical Approaches}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Space-Time Finite Element Method}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Domain-Decomposition Method}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Time-Decomposition Method}{3}{section.2.3}
\contentsline {part}{References}{4}{section*.7}
\contentsline {part}{Appendices}{5}{section*.9}
\contentsline {chapter}{\numberline {A}Semper Malesuada}{5}{appendix.A}
\contentsline {section}{\numberline {A.1}Sed Non Nibh}{5}{section.A.1}
\contentsline {chapter}{\numberline {B}Eleifend Elementum}{6}{appendix.B}
