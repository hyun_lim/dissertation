Welcome to the SDSU Thesis LaTeX template.


FAQ: 
Q:  How do I use this thing?

A:  The easiest way: copy and paste. 
    1.  Copy the SDSUThesis.cls file and all files from the ExampleThesis
        folder into your thesis/dissertation folder.
    2.  Rename SDSUExample.tex and SDSUExample.bib to something like
        "MyNameThesis.tex" and "MyNameThesis.bib" (the first part of the
        name must match) so you don't get confused later. Do not rename
        the SDSUThesis.cls file.
    3.  Edit the main LaTeX file--the one now named MyNameThesis.tex--to
        suit the needs of your thesis/dissertation. Most of the necessary
        changes are documented inside the comments in this file.
        Prerequisite: READ THE COMMENTS INSIDE THIS FILE! THEY INCLUDE
        DETAILED INSTRUCTIONS!
        Some changes you MUST make:
        a.  Change the "phdcss" parameter if needed.
        b.  Add the \usepackage commands your document needs.
        c.  Change the Document Variables (title, author, month,
            year, advisors, etc.)
        d.  Write some Acknowledgments or comment it out.
        e.  Paste your abstract into the Abstract area.
        f.  If you do not have tables/figures in your document, comment out
            the listoftables/listoffigures commands.
        g.  Write your chapters in separate files (see the example chapters
            for how that is done). Find where the example chapters are
            included and replace those chapters with your own. Add more 
            \input commands as needed.
        h.  Update the \bibliography command to match your new file name.
        i.  If you do not have appendix chapters, comment out the appendix-
            elated commands. If you do, change/add the \input commands.
    4.  Edit the bib file as needed.
    5.  Never, ever, ever change the SDSUThesis.cls file! Don't even open it
        unless you know what you are doing.
    6.  To get the final document, build the "MyNameThesis.tex" file.
