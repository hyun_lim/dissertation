\contentsline {subsection}{LIST OF FIGURES}{v}
\contentsline {subsection}{LIST OF TABLES}{x}
\contentsline {subsection}{ABSTRACT}{xii}
\contentsline {subsection}{1. \MakeUppercase {Introduction $\&$ Literature Review}}{1}
\contentsline {subsection}{2. \MakeUppercase {Physical Backgrounds and Implementation Issues}}{4}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 2.1\kern .5em \MakeUppercase {Klein-Gordon Equation}}{4}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 2.2\kern .5em \MakeUppercase {Critical Phenomena}}{8}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 2.3\kern .5em \MakeUppercase {Semilinear Wave Equation}}{9}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 2.4\kern .5em \MakeUppercase {Issues with Explicit Method : Numerical Tests for Klein-Gordon Equation}}{11}
\contentsline {subsection}{3. \MakeUppercase {TIME Decomposition Method for Hyperbolic PDEs}}{17}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 3.1\kern .5em \MakeUppercase {Domain-Decomposition Method for elliptic PDEs}}{17}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 3.2\kern .5em \MakeUppercase {Domain-Decomposition Method for hyperbolic PDEs}}{18}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 3.3\kern .5em \MakeUppercase {Time-Decomposition Method for hyperbolic PDEs}}{21}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 3.4\kern .5em \MakeUppercase {Comparison results DD with TD}}{25}
\contentsline {subsection}{4. \MakeUppercase {Numerical Procedures}}{31}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 4.1\kern .5em \MakeUppercase {Klein-Gordon Equation}}{31}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 4.2\kern .5em \MakeUppercase {Semilinear Wave Equation}}{33}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 4.3\kern .5em \MakeUppercase {Construct Element Stiffness Matrices}}{37}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 4.4\kern .5em \MakeUppercase {Time Decomposition Preconditoner}}{46}
\contentsline {subsection}{5. \MakeUppercase {Parallel Implementation}}{48}
\contentsline {subsection}{6. \MakeUppercase {Numerical Results}}{54}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 6.1\kern .5em \MakeUppercase {Klein-Gordon Equation}}{54}
\contentsline {subsection}{\hbox {}\hskip 1em\relax 6.2\kern .5em \MakeUppercase {Semilinear Wave Equation}}{59}
\contentsline {subsection}{7. \MakeUppercase {Conclusion $\&$ Future works}}{81}
