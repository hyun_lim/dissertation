\documentclass[prd]{revtex4}
\usepackage{amsmath, graphicx}
\usepackage{grffile}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{epsfig}
\usepackage{mathrsfs}  %  package for the "curly" fonts 
\usepackage{subfigure}
\usepackage{multirow}
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{algorithmicx}
\usepackage{amssymb}
\usepackage{tensor}
\usepackage{mathrsfs}

\newcommand*\apost{\textsc{\char13}}

\topmargin 0.30in
\textheight 9.00in

 \addtolength{\voffset}{-2cm}
 
\begin{document}

\title{Shift Condition for Unequal Mass Black Hole Binaries}

\author{Hyun Lim}


%\pacs{}
\maketitle

\section{$\Gamma$-freezing Condition}

In the BSSN formulation, an elliptic shift condition is obtained by imposing the $\partial_t \tilde{\Gamma}^k=0$ or using
\begin{align}
\label{eqn:g-freeze}
\tilde{\gamma}^{jk} \partial_j \partial_k \beta^i + \frac{1}{3} \tilde{\gamma}^{ij} \partial_i \partial_k \beta^k - \tilde{\Gamma}^j \partial_j \beta^i + \frac{2}{3} \tilde{\Gamma}^i \partial_j \beta^j + \beta^j \partial_j \tilde{\Gamma}^i \nonumber \\
- 2 \tilde{A}^{ij} \partial_j \alpha - 2\alpha \left(\frac{2}{3} \tilde{\gamma}^{ij} \partial_j K - 6 \tilde{A}^{ij} \partial_j \phi - \tilde{\Gamma}^i_{jk} \tilde{A}^{jk} \right) = 0
\end{align}
Once we have solved the previous elliptic equations for the shift, the $\Gamma$-freezing condition can be enforced at an analytic level by simply not evolving the $\tilde{\Gamma}^k$. Like the lapse condition, we obtain parabolic and hyperbolic shift prescriptions by making either $\partial_t \beta^i$ or $\partial^2_t \beta^i$ proportional to $\partial_t \tilde{\Gamma}^i$. This is called $\Gamma$-driver conditions. The parabolic $\Gamma$-driver condition has the form
\begin{align}
\label{eqn:ellip-g}
\partial_t \beta^i = f_p \partial_t \tilde{\Gamma}^i
\end{align}
where $f_p$ is a positive function of space and time. We also have hyperbolic $\Gamma$-driver condition
\begin{align}
\label{eqn:hyper-g1}
\partial^2_t \beta^i = f \partial_t \tilde{\Gamma}^i - \eta \partial_t \beta^i
\end{align}
or alternatively
\begin{align}
\label{eqn:hyper-g2}
\partial^2_t \beta^i = f \partial_t \tilde{\Gamma}^i - \left(\eta -\frac{\partial_t f}{f} \right) \partial_t \beta^i
\end{align}
where $f$ and $\eta$ are positive functions of space and time. For the hyperbolic $\Gamma$-driver condition, it is crucial to add a dissipation term with coefficient $\eta$ to avoid strong oscillations in the shift. We can manage to almost freeze the evolution of the system at late times by tuning the value of this dissipation coefficient.

In this work, we prefer to use Eqn~\ref{eqn:hyper-g2} form. There are several aspects about this choice. First, if F is time independent, the two choices (Eqn~\ref{eqn:hyper-g1} and Eqn~\ref{eqn:hyper-g2}) are identical. However, we typically choose $f$ to be function of $\alpha$. Anticipating a collapsing lapse near the black hole this implies that the term $f \partial_t \tilde{\Gamma}^i$ approaches zero and the evolution of the shift tends to freeze independent of behavior and numerical errors of $\partial_t \Gamma^i$. We implement the first choice of the $\Gamma$-driver, Eqn~\ref{eqn:hyper-g1}, as
\begin{align}
\label{eqn:hyper-g12}
\partial_t \beta^i &= B^i \\
\partial_t B^i &= f \partial_t \tilde{\Gamma}^i - \eta B^i
\end{align}
and the second choice, Eqn~\ref{eqn:hyper-g2}, as 
\begin{align}
\label{eqn:hyper-g22}
\partial_t \beta^i &= f B^i \\
\partial_t B^i &= \partial_t \tilde{\Gamma}^i - \eta B^i
\end{align}
The second variant has the advantage that if $f$ approaches zero due to the collapse of the lapse near a black hole, then $\partial_t \beta^i$ also approaches zero and the shift freezes. With the first variant, on the other hand, it is only $\partial_t B^i = \partial^2_t \beta^i$ that approaches zero, which means the shift can still evolve. Both $\Gamma$-drivers can give stable black hole evolutions, although the second one leads to less evolution near the black hole. 

We can further generalize gauge condition by introducing free parameters such that
\begin{align}
\label{eqn:hyper-gen1}
\partial_t \beta^i &= \lambda_2 \beta^j \partial_j + \frac{3}{4} f B^i \\ 
\label{eqn:hyper-gen2}
\partial_t B^i &= \partial_t \tilde{\Gamma}^i - \eta B^i + \lambda \beta^j \partial_j B^i - \lambda_4 \beta^j \partial_j \tilde{\Gamma}^i
\end{align}
where $\lambda$s are parameters that can be freely chosen and we usually choose $f$ as linear function of $\alpha$.

\section{Dynamical Shift Condition}

From examining the physical dimensions, $\eta$ needs to carry units, $[\eta] = 1/M$ where $M$ is the total mass of the space time under consideration. In simulations of black hole binaries with total mass $M=M_1+M_2$, we usually set $\eta = 2/M$ which has been found to work well in equal mass binaries simulations. For unequal mass binaries, the different black holes tolerate different range of $\eta$ which cannot be accomplished simultaneously for unequal masses using a constant value of $\eta$.
To overcome the conflicts between punctures with different masses in evolutions of two or more black holes, we suggest to construct a non-constant, position-dependent $\eta$ which knows about the position and mass of each puncture and takes a suitable value at every grid point. (See arxiv.org/0912.3125 and arxiv.org/1009.0292 for complete discussion about this)

Since we use the BSSN formalism, we want the form of $\eta$ to depend only BSSN variables in a way that does not change the principal part of the differential operator. 
Here, we choose to use conformal factor $\psi$, which contains information about the locations and masses of the punctures. (We consider usual conformal transformation $\tilde{\gamma}_{ij} = \psi^{-4} \gamma_{ij}$). The particular form of the damping term $\eta$ is
\begin{align}
\label{eqn:nonconst-eta}
\eta(x^k,t) = R_0 \frac{\sqrt{\tilde{\gamma}^{ij} \partial_i \psi^{-2} \partial_j \psi^{-2}}}{(1-\psi^{-2})^2}
\end{align} 
where $R_0$ is a dimensionless constant. We choose $\psi^{-2}$ to determine the position of the punctures. For a single Schwarzschild puncture of mass $M$ located at $r=0$, Eqn~\ref{eqn:nonconst-eta} leads to
    \begin{equation}
       \eta \rightarrow 
        \begin{cases}
            1/M & r \rightarrow 0 \\
            R_0/M & r \rightarrow \infty \\
        \end{cases}
    \end{equation}
Note that using Eqn~\ref{eqn:nonconst-eta} in Eqn~\ref{eqn:hyper-gen2} does not affect the principal part of Eqn~\ref{eqn:hyper-gen2}. Therefore, the system remains strongly hyperbolic, same as for constant $\eta$ case

In the code, we use the $\chi$ as evolved conformal factor ($\chi = \psi^{-4}$). We can use this idea to generalize non-constant $\eta$ term 
 \begin{align}
\eta(x^k,t) = R_0 \frac{\sqrt{\tilde{\gamma}^{ij} \partial_i \chi \partial_j \chi}}{(1-\chi^{a})^b}
\end{align} 
where $a$ and $b$ are free parameter we can choose. Changing $a$ and $b$ provides different behavior of damping term. Previous study chose $a=2$ and $b=2$, which reduces $\eta$ by a factor of 4 at infinity

\subsection{Purely Analytic Expression for Dynamical Damping}

Previously, we considered the gauge term $\eta$ begin amplified as they travel outwards and producing noise at mesh refinement boundaries to be severe enough to not pursue this approach further.

Since we always know the location of a puncture and we know its associated mass, we instead chose a form of damping that uses this local information throughout the domain. We can choose purely analytic expressions

\begin{align}
\label{eqn:anal-damp1}
\eta(x^k,t) = A+ \frac{C_1}{1+ w_1 (\hat{r}_1^2)^n}+\frac{C_2}{1+ w_1 (\hat{r}_2^2)^n}
\end{align}
and

\begin{align}
\label{eqn:anal-damp2}
\eta(x^k,t) = A+ C_1 e^{-w_1  (\hat{r}_1^2)^n} + C_2 e^{-w_2  (\hat{r}_2^2)^n}
\end{align}

$w_1$ and $w_2$ are positive and dimensionless parameters that can be chosen to change the width of the functions. The power $n$ is a positive integer which determines the fall-off rate. The constant $A$, $C_1$, and $C_2$ are then chosen to provide the desired values of $\eta$ at the punctures and at infinity. Lastly, $\hat{r}_1$ and $\hat{r}_2$ are defined as $\hat{r}_i = \frac{|r_i-r|}{|r_1-r_2|}$ where $i \in \{1,2\}$ and $r_i$ is the position of the $i$th black hole. The definition of $\hat{r}_i$ is chosen to naturally scale the fall-off of Eqns~\ref{eqn:anal-damp1} and~\ref{eqn:anal-damp2} to the separation of the black holes. In order to vie the damping factor units of inverse mass, we choose $A=2/M$, where $M$ is sum of the irreducible mass, then we take $C_i = 1/M_i-A$. In the equal mass case, the $C_i$ vanish and both Eqns~\ref{eqn:anal-damp1} and~\ref{eqn:anal-damp2} will give a constant value of $\eta$. After the formation of a common apparent horizon, when the punctures are very close to each other, we can switch back to using a constant value of $\eta$ on the whole grid in order to avoid singularities in Eqns~\ref{eqn:anal-damp1} and~\ref{eqn:anal-damp2} when $r_1 \simeq r_2$.

Note that using Eqn~\ref{eqn:anal-damp1} has more beneficial than using Eqn~\ref{eqn:anal-damp2} because Gaussian functions are more expensive to compute numerically than rational functions are. Further, Eqn~\ref{eqn:anal-damp1} is very similar to Eqn (13) suggested in arxiv.org/1003.0859

\end{document}
