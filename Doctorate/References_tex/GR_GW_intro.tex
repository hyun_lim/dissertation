\documentclass[prd]{revtex4}
\usepackage{amsmath, graphicx}
\usepackage{grffile}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{epsfig}
\usepackage{mathrsfs}  %  package for the "curly" fonts 
\usepackage{subfigure}
\usepackage{multirow}
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{algorithmicx}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{tensor}
\usepackage{xcolor}
\usepackage[math]{cellspace}
\usepackage{bookmark}
\usepackage{upquote}


% For mathcal like font that has lower case calligraphic font
\DeclareFontFamily{OT1}{pzc}{}
\DeclareFontShape{OT1}{pzc}{m}{it}{<-> s * [1.10] pzcmi7t}{}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

% Graphic path
\graphicspath{{Figures-GW/}}


\newcommand*\apost{\textsc{\char13}}

\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{%
  \edef\arraystretch{#1}%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols c}}
\makeatother


\topmargin 0.30in
\textheight 9.00in

 \addtolength{\voffset}{-2cm}
 
\begin{document}

\title{Basic Introduction to Generation of Gravitational Waves : Theory and Example}

\author{Hyun Lim}


%\pacs{}
\maketitle
The purpose of this note is that introduce basic gravitational wave
theory with detail calculations. As an example, we consider simple
binary inspiral for a source of gravitational wave. Also, gravitational
wave extraction from Weyl scalar is introduced. Here, we assume
that the reader has basic knowledge about differential geometry and
general relativity

\section{Generation of Gravitational Wave : Basic}

\subsection{Gravitational Wave Solution in Linearized Gravity}

\subsubsection{Preliminary Computations}

We consider linearized gravity i.e. metric can be written as $g_{ab} = \eta_{ab} + h_{ab}$, where $|| h_{ab} || <<1$. Then, the Christoffel symbols can be computed in the order of $\mathcal{O}(h)$
\begin{align}
\Gamma^a_{\,\,\, bc} &= \frac{1}{2}g^{ad} ( \partial_c g_{bd} + \partial_b g_{cd} - \partial_d g_{bc} ) \nonumber \\
			 &= \frac{1}{2}(\eta^{ad} - h^{ad})[\partial_c ( \eta_{bd} + h_{bd})+ \partial_b ( \eta_{dc} + h_{dc}) - \partial_d (\eta_{bc} + h_{bc} ) \nonumber  \\
			 &= \frac{1}{2} \eta^{ad} ( \partial_c h_{bd} + \partial_b h_{dc} - \partial_d h_{bc} ) + \mathcal{O}(h^2) \nonumber \\
			 &\simeq \frac{1}{2} ( \partial_c h_b^a + \partial_b h^a_c - \partial^a h_{bc} )
\end{align}
We consider only $\mathcal{O}(h)$ so Christoffel symbol square terms in Riemann tensors can be neglected because $\Gamma = \mathcal{O}(h)$
\begin{align}
R^a_{\,\,\, bcd} &= \partial_c \Gamma^a_{\,\,\, bd} - \partial_d \Gamma^a_{\,\,\, bc} + \mathcal{O}(h^2) \nonumber \\
		       &\simeq \frac{1}{2} \partial_c ( \partial_d h_a^b + \partial_b h^a_d - \partial^a h_{bd} )- \frac{1}{2} \partial_d ( \partial_c h_b^a + \partial_b h^a_c - \partial^a h_{bc}) \nonumber \\
		       &=\frac{1}{2}(\partial_c \partial_b h^a_d + \partial_d \partial^a h_{bc} - \partial_c \partial^a h_{bd} - \partial_d \partial_b h^a_c) \label{eqn:lin:Riem}
\end{align}
and the Ricci tensors are
\begin{align}
R_{bd} &= R^a_{\,\,\, bad} = \frac{1}{2} (\partial_a \partial_b h^a_b + \partial_d \partial^a h_{ba} - \partial_a \partial^a h_{bd} - \partial_d \partial_b h^a_a ) \nonumber \\
            &=\frac{1}{2} (\partial_a \partial_b h^a_b + \partial_d \partial^a h_{ba} -  \Box h_{bd} - \partial_d \partial_b h ) \label{eqn:lin:Ricci}
\end{align}
where $\Box = \partial_a \partial^a$ which is d\apost Alembertian operator and $h= h^a_a$. Then, Ricci scalar is
\begin{align}
R &= g^{ab} R_{ab} = \frac{1}{2} (\eta^{ab} - h^{ab}) (\partial_c \partial_a h^c_a + \partial_b \partial^c h_{ac} -  \Box h_{ab} - \partial_a \partial_b h ) \nonumber \\
    &\simeq \frac{1}{2} (\eta^{ab} \partial_c \partial_a h^c_a + \eta^{ab} \partial_b \partial^c h_{ac} - \eta^{ab} \Box h_{ab} - \eta^{ab} \partial_a \partial_b h ) \nonumber \\
    &= \frac{1}{2} (\partial_c \partial^b h^c_a +  \partial_b \partial^c h^b_{c} - \Box h - \partial^b \partial_b h ) \nonumber \\
    &= \partial_c \partial^a h^c_a - \Box h \label{eqn:lin:scalar}
\end{align}
Einstein tensor in this limit is
\begin{align}
G_{ab} &= R_{ab} - \frac{1}{2} g_{ab} R = \nonumber \\
            &= \frac{1}{2} ( \partial_c \partial_b h^c_a + \partial_a \partial^c h_{bc} - \Box h_{ab} - \partial_a \partial_b h) -\frac{1}{2} \eta_{ab} (\partial_c \partial^a h^c_a - \Box h) + \mathcal{O}(h^2) \nonumber \\
	    &\simeq \frac{1}{2} (\partial_c \partial_b h^a_a + \partial_a \partial^c h_{bc} - \Box h_{ab} - \partial_a \partial_b h -\eta_{ab} \partial_c \partial^d h^c_d + \eta_{ab} \Box h) \label{eqn:linein}
\end{align}

\subsubsection{Trace-reversed Perturbation}
Even linearized approximation, Eqn.~\ref{eqn:linein} is still
complicated. We usually write perturbation term $h_{ab}$ using
trace-reversed perturbation variable such that
\begin{align}
\bar{h}_{ab} = h_{ab} - \frac{1}{2} \eta_{ab} h \,\,\,\,\,\,\, \longleftrightarrow \,\,\,\,\,\,\, h_{ab} = \bar{h}_{ab} - \frac{1}{2} \eta_{ab} \bar{h}
\end{align}
Then, the Einstein tensors become
\begin{align}
G_{ab} &= \frac{1}{2} \left[\partial_c \partial_b \left( \bar{h}^c_b + \frac{1}{2} \eta^c_a h \right) + \partial_a \partial^c \left(\bar{h}_{bc} + \frac{1}{2} \eta_{bc} h \right) - \Box \left(\bar{h}_{ab} + \frac{1}{2} \eta_{ab} h \right) + \eta_{ab} \Box h \right] \nonumber \\
            &=\frac{1}{2} (\partial_c \partial_b \bar{h}^c_a + \partial_a \partial^c \bar{h}_{bc} - \Box \bar{h}_{bc} - \eta_{ab} \partial_c \partial^d \bar{h}^c_d )
\end{align}

\subsubsection{Gauge Fixing}
We still have a system of 10 PDEs for 10 variables $\bar{h}_{ab}$
in the limit of weak gravity. Here, we make a small gauge transformation.
To begin with, consider a general infinitesimal coordinate
transformation as $x \apost ^a  = x^a + \xi^a$ then
\begin{align}
g\apost_{ab}  = \eta \apost_{ab} + h \apost_{ab} = \frac{\partial x^c}{\partial x \apost^a} \frac{\partial x^d}{\partial x \apost^b} g_{cd} = \frac{\partial x^c}{\partial x \apost^a} \frac{\partial x^d}{\partial x \apost^b} (\eta_{cd} + h_{cd})
\end{align}
which yields 
\begin{align}
\eta_{cd} + h_{cd}  &= (\delta^a_c + \partial_c \xi^a)(\delta^b_d + \partial_d \xi^d) (\eta \apost_{ab} + h \apost_{ab}) \nonumber \\
			     &= \eta_{cd} + \partial_c \xi_d + \partial_d \xi_d + h \apost_{cd} + \mathcal{O}(\xi h) 
\end{align}
Then we have
\begin{align}
h \apost_{ab} &= h_{ab} - 2 \partial_{(a}\xi_{b)}
\end{align}
Therefore the trace-reversed metric is transformed as
\begin{align}
\bar{h} \apost_{ab} &= h \apost_{ab} - \frac{1}{2} \eta_{ab} h \apost \nonumber \\
			      &=h_{ab} - 2 \partial_{(a} \xi_{b)} - \frac{1}{2} \eta_{ab} ( h - 2 \partial^c \xi) \nonumber \\
			      &=h_{ab} - \frac{1}{2} \eta_{ab} h - 2 \partial_{(a} \xi_{b)}  + \eta_{ab} \partial^c \xi_c \nonumber \\
			      &=\bar{h}_{ab} - 2 \partial_{(a} \xi_{b)}  + \eta_{ab} \partial^c \xi_c
\end{align}
We observe that the Lorentz gauge $\partial^a \bar{h}_{ab} = 0$ holds if $\partial \bar{h} \apost_{ab} = \partial^a \bar{h}_{ab} - \Box \xi_b=0$. In other words, whenever $\partial^a \bar{h}_{ab} = \Box \xi_b$, we always put into a Lorentz gauge and one can always find solution to $\partial^a \bar{h}_{ab} = \Box \xi_b$/

Thus, in Lorentz gauge, the Einstein tensor can be simplified to be
\begin{align}
G_{ab} = - \frac{1}{2} \Box \bar{h}_{ab}
\end{align}

\subsubsection{Linearized Einstein Equations}
From previous section, the vacuum Einstein equations of the linearized gravity in the Lorentz gauge are
\begin{align}
\Box \bar{h}_{ab} = 0
\end{align}
This is a simple hyperbolic PDE, such as wave equation, which has a plane wave solution of
\begin{align}
\bar{h}_{ab} (\vec{x},t) = \mathpzc{Re}\left[ \int d^3 k A_{ab}(\vec{k}) e^{i (\vec{k} \cdot \vec{x} - \omega t)} \right]
\end{align}
where $\omega = | \vec{k} |$ as in a analogy of electromagnetic theory. Plugging this into the linearized Einstein equations gives
\begin{align}
k^a k_a = 0
\end{align}
so $k^a$ is a null vector which propagates a the speed of light. On the other hand, the Lorentz gauge produces $A_{ab} k^a=0$, implying $A_{ab}$ is orthogonal to $k^a$
\subsubsection{Transverse-Traceless Gauge}
In principle, $A_{ab}$ has six degrees of freedom, however dour of
them can be pinned down with an additional gauge fixing. One is the
transverse gauge choice which is $h^{TT}_{0a}$, that determines
three degree of freedom. Another is traceless gauge condition
$h\indices{^a_a^{TT}}= 0 $, fixing one degree of freedom. These
gauge choices leave us with only two propagating degrees of freedom
of the plane wave, which characterize two sorts of polarization of
gravitational waves, called plus-and cross-polarization.

Assuming that the wave propagation in the $z$-direction $k^a = (\omega,0,0,k_z)$, and $k^a$ is null vector so we have
\begin{align}
k_0 k^0 + k_i k^i &= 0 \\
\omega^a &= k_z^2
\end{align}
so we get again $k^a = (\omega,0,0,\omega)$. Therefore, $A_{ab} k^a = 0$ gives 

\begin{align}
k_z A_{za} + k_0 A_{0a} = k_z A_{za} = 0
\end{align}

since $A_{0a} = 0$ due to the transverse gauge condition. This implies that there are the only non-vanishing components of $A_{ab} : A_{11}, A_{12}, A_{21}, A_{22}$. In addition, due to the traceless and symmetric condition ($A_{11} = - A_{22}$ and $A_{12} = A_{21}$), the polarization tensor with two independent components are:

\begin{align}
A_{ab}=
\begingroup % keep the change local
\setlength\arraycolsep{8pt} % Column spacing
\begin{pmatrix}[1.5] % Row spacing
0 & 0 & 0 & 0\\
0 & h_{+} & h_{\times} & 0\\
0 & h_{\times} & h_{+} & 0\\
0 & 0 & 0 & 0
\end{pmatrix}
\endgroup
\end{align}
where $h_{+} = A_{11}$ and $h_{\times} = A_{12}$. Note that two components of $h_{+}$ and $h_{\times}$ completely characterize the plane wave propagation in the $z$-direction.

% Tensor package indices
%$R\indices{^a_{bcd}}$
%$\delta\indices{^a_b}$

\subsection{Geodesic Deviation}
Let us consider two particles with a separation vector $s^\mu$ in the presence of GWs in order to see the effect of GWs. Then the geodesic deviation equation for two particles is given by
\begin{align}
\label{eqn:geo-devi}
\frac{D^2 s^a}{D \tau^2} = R\indices{^a_{bcd}} v^b v^c s^d
\end{align}
Let $x^a = x^a (\tau,\nu)$ where $\tau$ is a proper time and $\nu$ is a distinct geodesic, then we have
\begin{align}
v^a = \frac{dx^a}{d\tau} \\
s^a = \frac{dx^2}{d\nu}
\end{align}
where $v^a$ is a tangent vector to the timelike geodesic and $s^a$ is a connecting vector that connects two neighboring curves. Now consider
\begin{align}
[v,s]^a &= v^b \partial_b s^a - s^b \partial_b v^a \nonumber \\
&=\frac{dx^a}{d\tau} \frac{\partial}{\partial x^b} \left(\frac{dx^a}{d\nu} \right) - \frac{dx^a}{d\nu} \frac{\partial}{\partial x^b} \left(\frac{dx^a}{d\tau} \right) \nonumber \\
&= \frac{d^2 x^a}{d \tau d\nu} - \frac{d^2 x^a}{d \nu d\tau} = 0 
\end{align}
By definition of Lie derivative, we also have a form
\begin{align}
[v,s]^a &= \pounds_v s^a \nonumber \\
&= v^b \partial_b s^a - s^b \partial_b v^a \nonumber \\
&=v^b \nabla_b s^a - s^b \nabla_b v^a \nonumber \\
&= \nabla_v s^a - \nabla_s v^a
\end{align}
Therefore, we conclude $\nabla_v s^a = \nabla_s v^a$. Applying $\nabla_v$ both side yields $\nabla_v \nabla_v s^a = \nabla_v \nabla_s v^a$. Recall a relation of Riemann tensor such that
\begin{align}
\nabla_X (\nabla_Y Z^a) - \nabla_Y (\nabla_X Z^a) - \nabla_{[X,Y]} Z^a = R\indices{^a_{bcd}}Z^b X^c X^d
\end{align}
Setting $X^a = Z^a = v^a$ and $Y^a = s^a$ gives
\begin{align}
\nabla_v (\nabla_s v^a) - \nabla_s (\underbrace{\nabla_v v^a}_{=0}) - \underbrace{\nabla_{[v,s]} v^a}_{=0} = R\indices{^a_{bcd}}v^b v^c s^d 
\end{align}
We know that $\nabla_v \nabla_v s^a = \nabla_v \nabla_s v^a$ so
\begin{align}
\nabla_v \nabla_v s^a = R\indices{^a_{bcd}}v^b v^c s^d 
\end{align}
which is geodesic deviation equation (Eqn.~\ref{eqn:geo-devi}) such that
\begin{align}
\frac{D^2 s^a}{D \tau^2} = \nabla_v \nabla_v s^a = R\indices{^a_{bcd}} v^b v^c s^d
\end{align}
Now we set $v^a = (1,0,0,0)$ with slowly moving approximation then we have
\begin{align}
\frac{D^2 s^a}{D \tau^2} \simeq \frac{\partial^2 s^a}{\partial t^2} = R\indices{^a_{00d}} s^d
\end{align}
From Eqn.~\ref{eqn:lin:Riem} in the TT-gauge, we get
\begin{align}
R\indices{^a_{00d}} &= \frac{1}{2} \left( \partial_0 \partial_0 h\indices{^a_d^{TT}} + \partial_d \partial^a \underbrace{h_{00}^{TT}}_{=0} - \partial_0 \partial^a \underbrace{h_{0d}^{TT}}_{=0} - \partial_d \partial_0 \underbrace{h\indices{^a_0^{TT}}}_{=0} \right) \nonumber \\
&= \frac{1}{2} \partial_0^2 h\indices{^a_d^{TT}}
\end{align}
Thus we have
\begin{align}
\label{eqn:geo-dev:lin}
\frac{\partial^2 s^a}{\partial t^2} = \frac{1}{2} s^a \partial_t^2 h\indices{^a_d^{TT}}
\end{align}

\subsection{Gravitational Wave Polarization}
\subsubsection{Plus Polarization : $h_{\times} = 0$}

From Eqn.~\ref{eqn:geo-dev:lin}
\begin{align}
\partial_t^2 s^1 = \frac{1}{2} s^1 \partial_t^2 (h_{+} e^{ik_\sigma x^\sigma} ) \label{eqn:geo-dev:lin:part1}\\ 
\partial_t^2 s^2 = -\frac{1}{2} s^2 \partial_t^2 (h_{+} e^{ik_\sigma x^\sigma} ) \label{eqn:geo-dev:lin:part2}
\end{align}
For Eqn.~\ref{eqn:geo-dev:lin:part1}, assume $s^1(t) = s^1(0) + \delta s^1(t)$ where $||s^1(0)|| >> ||\delta s^1(t)||$ then
\begin{align}
&\partial_t^2 (s^1(0) + \delta s^1(t)) = \frac{1}{2} (s^1(0) + \delta s^1(t)) \partial_t^2 (h_{+} e^{ik_\sigma x^\sigma} ) \nonumber \\
&\Rightarrow \partial_t^2 \delta s^1(t) \simeq \frac{1}{2} s^1(0) \partial_t^2 (h_{+} e^{ik_\sigma x^\sigma} )
\end{align}
Thus we can conclude
\begin{align}
\delta s^1 (t) = \frac{1}{2} s^1(0) h_{+} e^{i k_\sigma x^\sigma}
\end{align}
Therefore
\begin{align}
s^1(t) = s^1(0) \left( 1+ \frac{1}{2} h_{+} e^{i k_\sigma x^\sigma} \right)
\end{align}
Similarly, from Eqn.~\ref{eqn:geo-dev:lin:part2}, assume assume $s^2(t) = s^2(0) + \delta s^2(t)$ where $||s^2(0)|| >> ||\delta s^2(t)||$ then follow exactly same way in $s^1(t)$ case. We get
\begin{align}
s^2(t) = s^2(0) \left( 1- \frac{1}{2} h_{+} e^{i k_\sigma x^\sigma} \right)
\end{align}
\subsubsection{Cross Polarization : $h_{+} = 0 $}
Like plus polarization case, from Eqn.~\ref{eqn:geo-dev:lin}, we have
\begin{align}
\partial_t^2 s^1 = \frac{1}{2} s^1 \partial_t^2 (h_{\times} e^{ik_\sigma x^\sigma} ) \\ 
\partial_t^2 s^2 = \frac{1}{2} s^2 \partial_t^2 (h_{\times} e^{ik_\sigma x^\sigma} ) 
\end{align}
Then assume $s^i (t) = s^i(0) + \delta s^i(t)$ where $||s^i(0)|| >> ||\delta s^i (t)||$ where $i=1,2$ (which is exactly same approximation as previous case), we can find
\begin{align}
s^1(t) = s^1(0) \left( 1+ \frac{1}{2} h_{\times} e^{i k_\sigma x^\sigma} \right) \\
s^2(t) = s^2(0) \left( 1+ \frac{1}{2} h_{\times} e^{i k_\sigma x^\sigma} \right)
\end{align}

These solutions are described in Fig.~\ref{fig:two-polar}

\begin{center}
\begin{figure}[h]
      \includegraphics[width=12.5cm]{twopolar} 
      \caption{Plus and cross polarization modes of GW}
      \label{fig:two-polar}
\end{figure}
\end{center}

\subsubsection{R-polarization}
The R-polarization is a circularly polarized mode that is defined by
\begin{align}
h_R \equiv \frac{1}{\sqrt{2}} (h_{+} + i h_{\times} ) \\
h_L \equiv \frac{1}{\sqrt{2}} (h_{+} - i h_{\times} ) 
\end{align}

R-polarization mode is described in Fig.~\ref{fig:r-polar}. Note that the individual particles do not travel around the ring. They just move in little epicycles.

\begin{center}
\begin{figure}[h]
      \includegraphics[width=12.5cm]{rpolar} 
      \caption{R-polarization mode of GW}
      \label{fig:r-polar}
\end{figure}
\end{center}


\subsection{Generation of Gravitational Wave}
Consider the linearized Einstein\apost s equations with matter source such that
\begin{align}
\Box \bar{h}_{ab} = - 16 \pi G T_{ab}
\end{align}
Since this is a wave equation with inhomogeneous term, we can express a solution using Green\apost s function such that
\begin{align}
\bar{h}_{ab} (x^c) = - 16 \pi G \int d^4 y G(x^c -y^c) T_{ab} (y^c)
\end{align}
where $G(x^c -y^c)$ is a Green\apost s function which satisfies
\begin{align}
\Box_x G(x^c -y^c) = \delta^{(4)} (x^c - y^c)
\end{align}
In this note, we do not discuss about obtaining Green\apost s function for wave equation. Further, our interests are GW propagation through spacetime so we only consider retarded Green\apost s function which has a form
\begin{align}
G(x^c -y^c) = -\frac{1}{4\pi | \vec{x} - \vec{y} |} \delta^{(4)}(| \vec{x} - \vec{y} | - (x^0 - y^0)) \Theta(x^0-y^0)
\end{align}
where $\Theta(x^0 - y^0)$ is step function. Using this, the solution becomes
\begin{align}
\bar{h}_{ab} (x^c) &= - 16 \pi G \int d^4 y G(x^c -y^c) T_{ab} (y^c) \nonumber \\
&= - 4 G \int d^4 y \frac{1}{| \vec{x} - \vec{y} |} \delta^{(4)}(| \vec{x} - \vec{y} | - (x^0 - y^0)) \Theta(x^0-y^0) T_{ab} (y^0,\vec{y}) \nonumber \\
&=- 4 G \int_{x^0>y^0} d^3 y \frac{1}{| \vec{x} - \vec{y} |} T_{ab} (x^0 - |\vec{x} - \vec{y}|,\vec{y}) \nonumber \\
\end{align}
here we define a retarded time as
\begin{align}
t_r \equiv = t - |\vec{x} - \vec{y} | = x^0 - |\vec{x} - \vec{y}|
\end{align}

To get more explicit form, we need few assumptions. First, we assume that the source is isolated and composed of non-relativistic matter. Second, we consider the source is fairly far-away. Now take the usual Fourier transformation (or say change time-domain integration to frequency-domain integration) such that
\begin{align}
\tilde{\phi} (\omega,\vec{x}) &= \frac{1}{\sqrt{2\pi}} \int dt e^{i \omega t} \phi(t, \vec{x}) \\
\phi (t,\vec{x}) &= \frac{1}{\sqrt{2\pi}} \int d\omega e^{-i \omega t} \tilde{\phi}(\omega, \vec{x})
\end{align}
Thus
\begin{align}
\tilde{\bar{h}}_{ab} &= \frac{1}{\sqrt{2\pi}} \int dt e^{i \omega t} \bar{h}_{ab} (t, \vec{x}) \nonumber \\
&=\frac{1}{\sqrt{2\pi}} 4G \int dt \int d^3 e^{i \omega t} \frac{1}{| \vec{x} - \vec{y} |} T_{ab} (x^0 - |\vec{x} - \vec{y}|,\vec{y}) \nonumber \\
&=\frac{4G}{\sqrt{2\pi}} \int dt_r \int d^3 e^{i \omega (t_r + |\vec{x} - \vec{y}|)} \frac{1}{| \vec{x} - \vec{y} |} T_{ab} (t_r,\vec{y}) \nonumber \\
&=4G \int d^3 y e^{i \omega |\vec{x} - \vec{y}|} \frac{1}{|\vec{x} - \vec{y}|} \underbrace{\frac{1}{\sqrt{2\pi}} \int d t_r e^{i \omega t_r} T_{ab}(t_r , \vec{y})}_{\tilde{T}_{ab} (\omega, \vec{y})} \nonumber \\
&=4G \int d^3 y \frac{e^{i \omega |\vec{x} - \vec{y}|}}{|\vec{x} - \vec{y}|} \tilde{T}_{ab} (\omega, \vec{y}) \label{eqn:h:freq}
\end{align}
From the assumptions, we consider only an isolated sources that is fairly far away i.e. $\delta r << r$ and we also consider a slowly moving source - most of the radiation emitted will be at frequencies $\omega$ such that $\delta r << 1/\omega$. This gives
\begin{align}
\frac{e^{i \omega |\vec{x} - \vec{y}|}}{|\vec{x} - \vec{y}|} \simeq \frac{e^{i \omega r}}{r}
\end{align}
Therefore, Eqn.~\ref{eqn:h:freq} can be written as
\begin{align}
\label{eqn:met:inter1}
\tilde{\bar{h}}_{ab} = 4 G \frac{e^{i \omega r}}{r}\int d^3 y \tilde{T}_{ab} (\omega, \vec{y})
\end{align}
Now applying the Lorenz gauge condition $\partial_a \bar{h}^{ab}=0$
\begin{align}
\partial_a \bar{h}^{ab} (t, \vec{x}) = \partial_a \left[\frac{1}{\sqrt{2 \pi}} \int d^3 y e^{-i \omega t} \tilde{\bar{h}}^{ab} (\omega, \vec{y}) \right] = 0
\end{align}
Here we take inverse Fourier transform for $\tilde{\bar{h}}^{ab}$. This gives
\begin{align}
\frac{1}{\sqrt{2 \pi}} \int d^3 y \partial_a(e^{-i \omega t} \tilde{\bar{h}}^{ab} (\omega, \vec{y})) = 0 
\end{align}
which yields
\begin{align}
&\partial_t (e^{-i \omega t} \tilde{\bar{h}}^{0b} (\omega, \vec{y})) + \partial_i (e^{-i \omega t} \tilde{\bar{h}}^{ib} (\omega, \vec{y})) =0 \nonumber \\
&\Rightarrow- i \omega e^{-i \omega t} \tilde{\bar{h}}^{0b} (\omega, \vec{y}) + e^{-i \omega t} \underbrace{\partial_t \tilde{\bar{h}}^{0b} (\omega, \vec{y})}_{=0}+ e^{-i \omega t} \partial_i \tilde{\bar{h}}^{ib} (\omega, \vec{y}) = 0
\end{align}
Thus we have
\begin{align}
\label{eqn:metric:rel}
\tilde{\bar{h}}^{0b} (\omega, \vec{y}) = \frac{1}{i \omega} \partial_i \tilde{\bar{h}}^{ib} (\omega, \vec{y})
\end{align}
Eqn.~\ref{eqn:metric:rel} tells us that the induced metric components are recursive i.e. $\tilde{\bar{h}}^{00}$ can be determined from $\tilde{\bar{h}}^{i0}$, again $\tilde{\bar{h}}^{0i}$ can be determined from $\tilde{\bar{h}}^{ij}$ which is spatial part of the induced metric. So, we only need to consider $\tilde{\bar{h}}^{ij}$

Now consider below
\begin{align}
\label{eqn:rhs:int}
\int d^3 y \tilde{T}^{ij} ( \omega,\vec{y}) = \int d^3 y \partial_k (y^i \tilde{T}^{kj} ) - \int d^3 y \, y^i (\partial_k \tilde{T}^{kj}) 
\end{align}
First integration in RHS of Eqn.~\ref{eqn:rhs:int} vanishes this is surface term after using divergence theorem (In general, we may need to consider boundary integration but here we neglect it because this does not affect to get gravitational wave radiation). To evaluate second integration, consider conservation equation $\partial_a T^{ab} = 0$ (Again, in general, we should use $\nabla_a T^{ab}$ for non-flat metric which is not the case we consider here)
\begin{align}
\partial_a T^{ab} = \frac{1}{\sqrt{2 \pi}} \int d^3 y \partial_a ( e^{-i \omega t} \tilde{T}^{ab} (\omega, \vec{y}) ) = 0
\end{align}
which gives
\begin{align}
\partial_a ( e^{-i \omega t} \tilde{T}^{ab} (\omega, \vec{y}) ) &= \partial_t ( e^{-i \omega t} \tilde{T}^{0b} (\omega, \vec{y}) ) + \partial_i ( e^{-i \omega t} \tilde{T}^{ib} (\omega, \vec{y}) )\nonumber \\
&=-i \omega e^{-i \omega t} \tilde{T}^{0b} (\omega, \vec{y})  + e^{-i \omega t} \underbrace{\partial_t \tilde{T}^{0b} (\omega, \vec{y})}_{=0}  + e^{-i \omega t } \partial_i \tilde{T}^{ib} (\omega, \vec{y})  = 0 
\end{align}
Therefore, we can conclude 
\begin{align}
\label{eqn:Tab:rel}
\partial _i \tilde{T}^{ib} = i \omega \tilde{T}^{ob}
\end{align}
Like Eqn.~\ref{eqn:metric:rel}, Eqn.~\ref{eqn:Tab:rel} tells us that the $\tilde{T}^{ab}$ are recursive i.e. $\tilde{T}^{00}$ can be determined from $\tilde{T}^{i0}$, again $\tilde{\bar{T}}^{0i}$ can be determined from $\tilde{T}^{ij}$. So it tells us what we have to know if just $\tilde{T}^{ij}$. Substitute Eqn.~\ref{eqn:Tab:rel} into the second integration in RHS of Eqn.~\ref{eqn:rhs:int} gives
\begin{align}
\int d^3 y \tilde{T}^{ij} &= - \int d^3y \, y^i \partial_k \tilde{T}^{kj} = - i \omega \int d^3 y \, y^i \tilde{T}^{0j}  \nonumber \\
&= - \frac{i \omega}{2} \int d^3 y (y^i \tilde{T}^{0j} + y^j \tilde{T}^{0i}) \nonumber \\
&= - \frac{i \omega}{2} \int d^3 y (\delta^j_k y^i \tilde{T}^{0k} + \delta^i_k y^j \tilde{T}^{0k}) \nonumber \\
&= - \frac{i \omega}{2} \int d^3 y [(\partial_k y^j) y^i \tilde{T}^{0k} + (\partial_k y^i) y^j \tilde{T}^{0k}] \nonumber \\
&= - \frac{i \omega}{2} \int d^3 y \left[\partial_k (y^i y^j \tilde{T}^{0k} ) - y^i y^j \partial_k \tilde{T}^{0k}\right] 
\end{align}
Similar as previous, first term vanishes because this is surface term and using Eqn.~\ref{eqn:Tab:rel} again, we have $\partial_k \tilde{T}^{0k} = i \omega \tilde{T}^{00}$ so
\begin{align}
\int d^3 y \tilde{T}^{ij} (\omega, \vec{y}) = - \frac{\omega^2}{2} \int d^3 y \, y^i y^j \tilde{T}^{00}(\omega,\vec{y})
\end{align}
We define quadrupole momentum tensor in frequency domain such that
\begin{align}
\tilde{\mathcal{I}}^{ij}(\omega) = \int d^3 \, y^i y^j \tilde{T}^{00} (\omega,\vec{y}) 
\end{align}
Based on this (also w.l.o.g), we can define the quadrupole momentum tensor 
\begin{align}
\label{eqn:quad-mom}
\mathcal{I}^{ij}(t) \equiv \int d^3 \, y^i y^j \tilde{T}^{00} (t,\vec{y}) 
\end{align}
Therefore, Eqn.~\ref{eqn:met:inter1} can be written as
\begin{align}
\tilde{\bar{h}}_{ij} (\omega,\vec{x}) &= 4G \frac{e^{i \omega r}}{r} \int d^3 y \tilde{T}_{ij} (\omega,\vec{y}) \nonumber \\
&= - 2 G \omega^2 \frac{e^{i \omega r}}{r} \tilde{\mathcal{I}}_{ij} (\omega)
\end{align}
Take inverse Fourier transform to get the metric in time domain
\begin{align}
\bar{h}_{ij} (t,\vec{x}) &= \frac{1}{\sqrt{2 \pi}} \int d \omega \, e^{-i \omega t} \tilde{\bar{h}}_{ij} (\omega, \vec{x}) \nonumber \\
&= - \frac{1}{\sqrt{2 \pi}} \int d \omega \, e^{-i \omega t}  2 G \omega^2 \frac{e^{i \omega r}}{r} \tilde{\mathcal{I}}_{ij} (\omega) \nonumber \\
&= - \frac{2 G}{\sqrt{2 \pi}r} \int d \omega \underbrace{\omega^2 e^{-i \omega t}}_{-\frac{d^2}{dt^2} e^{-i \omega t}} e^{i \omega r}  \tilde{\mathcal{I}}_{ij} (\omega) \nonumber \\
&=  \frac{2 G}{\sqrt{2 \pi}r} \int d \omega \frac{d^2}{dt^2} e^{-i \omega t} e^{i \omega r}  \tilde{\mathcal{I}}_{ij} (\omega) \nonumber \\
&= \frac{2G}{r} \frac{d^2}{dt^2} \overbrace{\left[\int d^3 y \underbrace{e^{-i\omega(t-r)}}_{e^-i \omega t_r} \tilde{\mathcal{I}}_{ij} (\omega) \right]}^{\mathcal{I}_{ij}(t_r)}
\end{align}
We finally get the quadrupole formula
\begin{align}
\label{eqn:quad-formula}
\bar{h}_{ij} (t,\vec{x}) = \frac{2G}{r} \frac{d^2}{dt^2} \mathcal{I}_{ij} (t_r)
\end{align}
where $\mathcal{I}_{ij}(t_r)$ is quadrupole momentum tensor in retarded time such that
\begin{align}
\label{eqn:quad-ten:ret-t}
\mathcal{I}^{ij} \equiv \int d^3 y \, y^i y^j T^{00} (t_r,\vec{y})
\end{align}
and $T^{00}$ is just an energy density

\subsubsection{Example : Simple Binary Star Model}
In this section, we calculate strain tensor with simple binary star model. We assume that binary star system is located in $x-y$ plane. Two stars have same mass $M$ and are equally distanced from center say $R$. In the Newtonian approximation of circular orbit, we get
\begin{align}
\frac{GM^2}{(2R)^2} = \frac{M v^2}{R^2}
\end{align}

\begin{center}
\begin{figure}[h]
      \includegraphics[width=12.5cm]{binary-xy} 
      \caption{A binary star system in a $x-y$ plane}
      \label{fig:binary-xy}
\end{figure}
\end{center}

This formula is showing nothing but $F_{Grav} = F_{Centrifugal}$. This gives a velocity such that
\begin{align}
v = \sqrt{\frac{GM}{4R}}
\end{align}
The time it takes to complete a single orbit is $T = 2 \pi R / v$ and the angular frequency is
\begin{align}
\Omega = \frac{2 \pi}{T} = \sqrt{\frac{GM}{4R^3}}
\end{align}
The coordinate system of each star is
\begin{align}
x^a &= R \cos \Omega t, \,\,\,\,\,  y^a = R \sin \Omega t,  \,\,\,\,\, z^a = 0 \\
x^b &= -R \cos \Omega t, \,\,\,\,\,  y^b = -R \sin \Omega t,  \,\,\,\,\, z^b = 0 
\end{align}
where indices $a$ and $b$ indicate current location of each star in Fig~\ref{fig:binary-xy}. Energy density can be written as in this case
\begin{align}
T^{00} (t, \vec{x}) &= M \left[\delta(x-x^a)\delta(y-y^a)\delta(z)+\delta(x-x^b)\delta(y-y^b)\delta(z) \right] \nonumber \\
&=M \left[\delta(x-R\cos \Omega t)\delta(y-R \sin \Omega t)\delta(z)+\delta(x+R \cos \Omega t)\delta(y+R \sin \Omega t)\delta(z) \right]
\end{align}
Using Eqn.~\ref{eqn:quad-ten:ret-t} (do not worry much about retarded time in here), we can calculate quadrupole momentum
\begin{align}
\mathcal{I}^{xx} &= \int d^3 y \, x x M \left[\delta(x-R\cos \Omega t)\delta(y-R \sin \Omega t)\delta(z)+\delta(x+R \cos \Omega t)\delta(y+R \sin \Omega t)\delta(z) \right] \nonumber \\
&= M (R^2 \cos^2 \Omega t + R^2 \cos^2 \Omega t )\nonumber \\
&= M R^2  (1+ \cos 2 \Omega t)\\
\mathcal{I}^{xy} &= \int d^3 y \, x y M \left[\delta(x-R\cos \Omega t)\delta(y-R \sin \Omega t)\delta(z)+\delta(x+R \cos \Omega t)\delta(y+R \sin \Omega t)\delta(z) \right] \nonumber \\
&= M (R^2 \cos \Omega t \sin \Omega t + R^2 \cos \Omega t \sin \Omega t) \nonumber \\
&= 2 M R^2 \cos \Omega t \sin \Omega t = M R^2 \sin 2 \Omega t= \mathcal{I}^{yx}\\
\mathcal{I}^{yy} &= \int d^3 y \, y y M \left[\delta(x-R\cos \Omega t)\delta(y-R \sin \Omega t)\delta(z)+\delta(x+R \cos \Omega t)\delta(y+R \sin \Omega t)\delta(z) \right] \nonumber \\
&= M (R^2 \sin^2 \Omega t + R^2 \sin^2 \Omega t )\nonumber \\
&= M R^2  (1- \cos 2 \Omega t)\\
\mathcal{I}^{iz} &= \mathcal{I}^{zi} = 0
\end{align}
Thus $\bar{h}^{ij}$ is
\begin{align}
\bar{h}^{ij} = \frac{2G}{r} \ddot{\mathcal{I}}^{ij} = \frac{8 M G}{r} R^2 \Omega^2
\begingroup % keep the change local
\setlength\arraycolsep{8pt} % Column spacing
\begin{pmatrix}[1.5] % Row spacing
-\cos 2 \Omega t & -\cos 2 \Omega t & 0 \\
-\sin 2 \Omega t & \cos 2 \Omega t & 0 \\
0  & 0 & 0 
\end{pmatrix}
\endgroup
\end{align}
Note that we need to have retarded time $t_r$ on element of RHS matrix above but here we think that retarded time is time for detector.



\subsection{Energy Loss from Gravitational Radiation}
\subsubsection{Weak Field Approximation : Revisit}
Again, we assume the weak field approximation of metric as $g_{ab} = \eta_{ab} + h_{ab}$ and keep $O(h^2)$ terms in metric perturbation. Further, we apply the traceless and transverse (TT) gauge such that
\begin{align}
\partial^a h^{TT}_{ab} = 0, \,\,\,\,\,\, h^{TT} = 0
\end{align}
From now, we drop the super(or sub)script of $TT$ for convenience. If we need, we notify specifically later.

\subsubsection{Averaged Bracket}

\subsubsection{Example : Simple Plain Wave}

\subsubsection{Rate of Energy Loss}

\subsubsection{Computing Power}

$\dddot{\mathcal{J}}$

\subsection{Binary Inspiral Sources}



% Beginning for Weyl scalar	
\section{Gravitational Waves Extraction using a Complex Weyl Scalar}

The Newman-Penrose formalism is a tetrad (vierbein) formalism in GR, in which four null vector fields (composed of two real and two complex vectors) are employed as the tetrad bases and then complex Weyl scalars are analyzed. This is widely used with 3+1 formalism

Here, I refer several papers and books including Numerical Relativity by Shibata and An Approach to Gravitational Radiation by a Method of Spin Coefficients by Newman and Penrose

\subsection{Newman-Penrose Formalism}

The Newman-Penrose formalism gives relations between Weyl tensor and Ricci rotation coefficients. Let $(e_a)^\alpha$ be the tetrad vector fields where $\alpha$ specifies a series of the tetrad bases with $\alpha = 1\cdots 4$

Here, $(e_\alpha)^a (e_\alpha)_a =0$ and $(e_\alpha)^a (e_\beta)_a = \bar{\eta}_{\alpha \beta}$ with $\bar{\eta}_{\alpha \beta}$ being a matrix composed of constant components and metric can be written

\begin{align}
g_{ab} = \bar{\eta}^{\alpha \beta} (e_{\alpha})_a (e_\beta)_b
\end{align}

Then the Ricci rotation coefficient, $\gamma_{\alpha \beta \sigma}$, is defined by
\begin{align}
\gamma_{\alpha \beta \sigma} = (e_\alpha)^a (e_\sigma)^\sigma \nabla_b (e_\beta)_a
\end{align}

Since $\gamma_{\alpha \beta \sigma} = - \gamma_{\beta \alpha \sigma}$, so it has twenty-four independent components in general.

The tetrad components of the Riemann tensor can be written in terms of the Ricci rotation coefficients by

\begin{align}
R_{\alpha \beta \sigma \lambda} &= R_{abcd} (e_\alpha)^a (e_\beta)^b (e_\sigma)^c (e_\lambda)^d \nonumber \\
						  &= - \partial_\lambda \gamma_{\alpha \beta \sigma} + \partial_\sigma \gamma_{\alpha \beta \lambda} \nonumber \\
						  &=-\bar{\eta}^{\mu \nu} [ \gamma_{\alpha \beta \mu} ( \gamma_{\sigma \nu \lambda} -  \gamma_{\lambda \nu \sigma}) + \gamma_{\beta \mu \sigma} \gamma_{\nu \alpha \lambda} - \gamma_{\beta \mu \lambda} \gamma_{\nu \alpha \sigma}]
\end{align}
Also, the Bianchi identity in the tetrad bases is written by
\begin{align}
 0 &= (\nabla_{[e} R_{cd]ab}) (e_\alpha)^a (e_\beta)^b (e_\sigma)^c (e_\rho)^e \nonumber \\
    &= \partial_{[\rho} R_{\sigma \lambda ] \alpha \beta} - \bar{\eta}^{\mu \nu} ( \gamma_{\mu \alpha [ \rho} R_{\sigma \lambda] \nu \beta} + \gamma_{\mu \beta [ \rho} R_{\sigma \lambda] \alpha \nu} - \gamma_{\mu [ \sigma  \rho} R_{\lambda ] \nu \alpha \beta} +  \gamma_{\mu [ \lambda  \rho} R_{\sigma ] \nu \alpha \beta} )
\end{align}
Newman and Penrose chose the following set of the tetrad bases
\begin{align}
l^a = (e_1)^a, \,\,\,\,\, k^a = (e_2)^a, \,\,\,\,\, m^a = (e_3)^a, \,\,\,\,\, \bar{m}^a = (e_4)^a
\end{align}
which satisfies
\begin{align}
- l_a k^a &= m_a \bar{m}^a = 1 \\
l_a m^a &= l_a \bar{m}^a = k_a m^a = k_a \bar{m}^a = 0
\end{align}

$\bar{m}^a$ indicates the complex conjugate of $m^a$. Here, $\bar{\eta}_{\alpha \beta}$ is written as

\begin{align}
\bar{\eta}_{\alpha \beta}=
\begingroup % keep the change local
\setlength\arraycolsep{8pt} % Column spacing
\begin{pmatrix}[1.5] % Row spacing
0 & -1 & 0 & 0\\
-1 &0 & 0 & 0\\
0 & 0 & 0 & 1\\
0 & 0 & 1 & 0
\end{pmatrix}
\endgroup
\end{align}
Associated with the four tetrad bases, four directional derivatives are defined by
\begin{align}
D = l^\alpha \partial_\alpha, \,\,\,\,\, \Delta = k^\alpha \partial_\alpha, \,\,\,\,\, \delta = m^\alpha \partial_\alpha, \,\,\,\,\, \bar{\delta} = \bar{m}^\alpha \partial_\alpha
\end{align}

In the vacuum case, the Newman-Penrose formalism reduces to the equations for the complex Weyl scalars and spin coefficients. Using the null tetrad-base components, Newman and Penrose defined the following five complex Weyl scalars
\begin{align}
\Psi_0 &= - C_{1313} = - C_{abcd} l^a m^b l^c m^d \\
\Psi_1 &= - C_{1213} = - C_{abcd} l^a k^b l^c m^d \\
\Psi_2 &= -\frac{1}{2} (C_{1212} - C_{1234} ) = - \frac{1}{2} C_{abcd} ( l^a k^b l^c k^d - l^a k^b m^c \bar{m}^d) \\
\Psi_3 &= C_{1224} = C_{abcd} l^a k^b k^c \bar{m}^d \\
\Psi_4 &= - C_{2424} = - C_{abcd} k^a \bar{m}^b k^c \bar{m}^d
\end{align}

Note that $C_{1314} = C_{2324} = C_{1323} = C_{1424} = 0$ and $C_{1212} = C_{3434}$ hole because of the tracefree property of the Weyl tensor i.e. $C_{abcd} g^{ac} = 0$. It is also shown that $C_{1334} = \Psi_1$, $C_{1324} = \Psi_2$, and $C_{2434} = - \Psi_3$. Then, the sixteen independent components of the Bianchi identity for vacuum spacetime is written as 
\begin{align}
-D \Psi_1 + \bar{\delta} \Psi_0
\end{align}

%\subsection{The gauge freedom of the Newman-Penrose Quantities and Classification of Spacetime}

\subsection{Weyl Tensors in the 3+1 Formalism}

\subsection{$\Psi_4$ and Outgoing Gravitational Waves}




\end{document}
