\documentclass[prd]{revtex4}
\usepackage{amsmath, graphicx}
\usepackage{grffile}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{epsfig}
\usepackage{mathrsfs}  %  package for the "curly" fonts 
\usepackage{subfigure}
\usepackage{multirow}
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{algorithmicx}
\usepackage{amssymb}
\usepackage{tensor}
\usepackage{xcolor}

\newcommand*\apost{\textsc{\char13}}

\topmargin 0.30in
\textheight 9.00in

 \addtolength{\voffset}{-2cm}
 
\begin{document}

\title{Black Holes in Einstein-Proca-Dilaton Theory}

\author{Hyun Lim}


%\pacs{}
\maketitle

\section{Mathematical Formulations of the Einstein-Proca Theory}

Before going Einstein-Proca-Dilaton, consider Einstein-Proca (EP) system first. There are certain different literatures about this, here we consider an action for EP theory

\begin{equation}
S=\int d^4 x \sqrt{-g} \left[\frac{R}{4} - \frac{1}{4}F^2 -\frac{\mu_v}{2} X^2 \right]
\end{equation}
where $F_{ab} = \nabla_a X_b - \nabla_b X_a$, $X_a$ is a vector field that couples with action, and $\mu_v = m_v/\hbar$. Note that we consider solely gravitational coupling to massive vector field, a purely gravitational effect that is independent of kinematic mixing between the hidden sector and visible photon.

\subsection{Equations of Motion in the EP theory}
From the action, we can derive equations of motion. For maxwell field,
\begin{align}
\nabla_a F^{ab} = - \mu_v X^a
\end{align}
For metric, 
\begin{align}
R_{ab} - \frac{1}{2} g_{ab} R = 2 F_{ac} F_b^{\,\,c} - \frac{1}{2} g_{ab} F^{cd} F_{cd} + \mu_v^2 ( 2 X_a X_b - g_{ab} X^c X_c)\label{ein:eqn}
\end{align}

In Einstein-Proca case, the vector $X_a$ is not pure gauge field unlike Maxwell theory. This implies that the Lorenz condition has to be satisfied such that
\begin{align}
\nabla_a X^a = 0
\end{align}

\subsection{The 3+1 Decomposition}
Here, we use usual 3+1 decomposition quantities:
\begin{align}
g_{ab}&=h_{ab}-n_a n_b \nonumber \\
n_a &= (-\alpha,0,0,0) \nonumber \\
n^a &= \left(\frac{1}{\alpha}, -\frac{\vec{\beta}}{\alpha} \right) \nonumber \\
K_{ab} &= - h_a^c h_b^d \nabla_c n_d \nonumber
\end{align}
where $h_{ab}$ is an induced metric on the 3D hypersurface $\Sigma$, $n_a$ is a normal vector to the $\Sigma$ that satisfies $n_a n^a =-1$ (timelike) and $K_{ab}$ is an extrinsic curvature on the $\Sigma$. Now, we need to consider Proca filed. We split the Proca field into its scalar $\chi_\varphi$ and 3-vector potential $\chi_i$ given by
\begin{align}
\chi_i &= h^a_{\,\, i} X_a \\
\chi_\varphi &= -n^a X_a \\
X_a &= \chi_a + n_a \chi_\varphi \\
\end{align}
Like Maxwell\apost s theory, we introduce electric and magnetic field
\begin{align}
\bot E_i &= h^a_{\,\,\,i}F_{ab}n^b \\
\bot B_i &= h^a_{\,\,\,i}(*F_{ab})n^b=\epsilon^{ijk}D_j \chi_k
\end{align}
where $(*F)$ is dual of Maxwell tensor and $D_i$ is spatial covariant derivative. By definition, we have $\bot E_a n^a = \bot B_a  n^a =0$. The $F_{ab}$ now can be re-written as
\begin{align}
F_{ab} = n_a \bot E_b - n_b \bot E_b + D_a \chi_b - D_b \chi_a
\end{align}
The decomposed equations of motions are
\begin{align}
\partial_t h_{ij} &= -2 \alpha K_{ij} + \pounds_\beta h_{ij} \\
\partial_t \chi_i &= - \alpha ( \bot E_i + D_i \chi_\varphi) - \chi_\varphi D_i \alpha + \pounds_\beta \chi_i \\
\partial_t E^i &= \alpha (K \bot E^i+D^iZ + \mu_v^2 \chi^i + \epsilon^{ijk} D_j \bot B_k) - \epsilon^{ijk} \bot B_j D_k \alpha + \pounds_\beta \bot E^i \\ 
\partial_t K_{ij} &= - D_i D_j \alpha + \alpha(R_{ij} - 2 K_{ik}K^k_{\,\,\, j} + K K_{ij}) + \pounds_\beta K_{ij} \nonumber \\
		       & + 2 \alpha \left(\bot E_i \bot E_j - \frac{1}{2} h_{ij} \bot E^k \bot E_k + \bot B_i \bot B_j - \frac{1}{2} h_{ij} \bot B^k \bot B_k - \mu_v^2 \chi_i \chi_j \right)\\
\partial_t \chi_\varphi &= - \chi^i D_i \alpha + \alpha (K \chi_\varphi - D_i \chi^i - Z) + \pounds_\beta \chi_\varphi \\
\partial_t Z &= \alpha(D_i \bot E^i + \mu_v^2 \chi_\varphi - \eta Z) + \pounds_\beta Z
\end{align}
here we introduce additional auxiliary scalar variable $Z$ to dampen violation of the Proca field equivalent of the Gauss constraint at some damping parameter $\eta>0$. We can recover original sets of equation by setting $Z=0$. And, usual momentum and Hamiltonian constraints are
\begin{align}
R + K^2 - K_{ij} K^{ij} &= J^i \\
D_j K^{ij} - D^i K &= 2 \rho 
\end{align}
where $\rho$ and $J^i$ are defined by using energy-momentum tensor in RHS of Eqn.~\ref{ein:eqn}
\begin{align}
\rho &= T_{ab}n^a n^b \\
       &= \epsilon_{ijk} \bot E^j \bot B^k + \mu_v^2 \chi_\varphi \chi_i\\
J^i & = -h^{ia}T_{ab}n^b \\
      &= 2 [\bot E^i \bot E_i + \bot B^i \bot B_i + \mu_v^2(\chi_\varphi^2 + \chi_i \chi^i)]
\end{align}
We can derive BSSN formalism from this. 
%BSSN start
\subsection{BSSN Formalism}
For going to BSSN, we consider a conformal transformation of the 3-metric
\begin{equation}
\tilde{\gamma}_{ij} = e^{-4 \bar{\phi}} h_{ij}
\end{equation}
where $\bar{\phi}$ is conformal factor such that the conformal metric has unit determinant. Thus, $h = exp(12^\phi)$ because $\tilde{\gamma}=1$. We also define a conformally rescaled traceless extrinsic curvature
\begin{equation}
\tilde{A}_{ij} = e^{-4 \bar{\phi}} \left(K_{ij} - \frac{1}{3} h_{ij} K \right)
\end{equation}
Finally, define conformal connection function
\begin{equation}
\tilde{\Gamma}^i = \tilde{\gamma}^{jk} \tilde{\Gamma}_{jk}^i = - \partial_j \tilde{\gamma}^{ij}
\end{equation}
where the $\tilde{\Gamma}_{jk}^i$ are the Christoffel symbols built out of the conformal 3-metric which is
\begin{equation}
\tilde{\Gamma}_{ij}^k = \Gamma_{ij}^k - 2 (\delta_i^k \partial_j \bar{\phi} + \delta_j^k \partial_i \bar{\phi} - \gamma_{ij} \gamma^{kl} \partial_l \bar{\phi})
\end{equation}
Also, we define $\bar{\chi} = e^{-4 \bar{\phi}}$ for convenient purpose. Using this, we can get the relations
\begin{align}
\tilde{\gamma}_{ij} = \bar{\chi} h_{ij} \\
\tilde{\gamma}^{ij} = \frac{1}{\bar{\chi}} h^{ij} \\
\bar{\phi} = -\frac{1}{4} \ln \bar{\chi}
\end{align}
Using that, the evolution equations for Proca, electric, and auxiliary fields are
\begin{align}
\partial_t \chi_i &= \pounds_\beta \chi_i - \frac{\alpha}{\bar{\chi}} \tilde{\gamma}_{ij} E^j - \alpha \partial_i \chi_\varphi - \chi_\varphi \partial_i \alpha \\
\partial_t \chi_\varphi &= \beta^i \partial_i \chi_\varphi + \alpha K \chi_\varphi - \alpha \tilde{\gamma}^{ij} \bar{\chi} \partial_j \chi_i + \alpha \bar{\chi} \chi_i \tilde{\Gamma}^i + \frac{\alpha}{2} \chi_i \tilde{\gamma}^{ij} \partial_j \bar{\chi} - \bar{\chi} \tilde{\gamma}^{ij} \chi_i \partial_j \alpha - \alpha Z \\
\partial_t Z &= \beta^i \partial_i Z + \alpha \partial_i E^i - \frac{3}{2 \bar{\chi}} \alpha E^i \partial_i \chi + \alpha \mu_v^2 \chi_\varphi - \alpha \eta Z \\
\partial_t \bot E^i &= \beta^j \partial_j \bot E^i - \bot E^j \partial_j \beta^i + \alpha K E^i + \alpha \mu_v^2 \bar{\chi} \tilde{\gamma}^{ij} \chi_j + \alpha \bar{\chi} \tilde{\gamma}^{ij} \partial_j Z + \bar{\chi}^2 \tilde{\gamma}^{ij} \tilde{\gamma}^{kl} \partial_l \alpha ( \partial_j \chi_k - \partial_k \chi_j) \nonumber \\
                            & + \alpha \bar{\chi}^2 \tilde{\gamma}^{ij} \tilde{\gamma}^{kl} (D_k \partial_j \chi_l - D_k \partial_l \chi_j) + \frac{\alpha}{2} \bar{\chi} \tilde{\gamma}^{ij} \tilde{\gamma}^{kl} (\partial_j \chi_l \partial_k \bar{\chi} - \partial_k \chi_j \partial_l \bar{\chi})
\end{align}
The evolution equation for the variable, $\tilde{\gamma}_{ij}$, $\tilde{\Gamma}^i$, $\tilde{A}_{ij}$, $\bar{\phi}$, and $K$ can be obtained from the decomposition of the Einstein tensor
\begin{align}
\partial_t \tilde{\gamma}_{ij} &= \beta^k \partial_k \tilde{\gamma}_{ij} + \tilde{\gamma}_{kj} \partial_i \beta^k -\frac{2}{3} \tilde{\gamma}_{ij} \partial_k \beta^k - 2 \alpha \tilde{A}_{ij} \\
\partial_t \bar{\phi} &= \beta^k \partial_k \bar{\phi} + \frac{1}{6} \partial_k \beta^k - \frac{1}{6} \alpha K \\
\partial_t \tilde{A}_{ij} &= \beta^k \partial_k \tilde{A}_{ij} + \tilde{A}_{kj} \partial_j \beta^k -\frac{2}{3} \tilde{A}_{ij} \partial_k \beta^k + e^{-4 \bar{\phi}} \left[ - D_i D_j \alpha + \alpha ^{(3)}R_{ij} - 8 \pi G \alpha \bot T_{ij}\right]^{TF} \nonumber \\
				&\indent + \alpha \left( K \tilde{A}_{ij} - 2 \tilde{A}_{ik} \tilde{A}_{j}^k \right) \\
\partial_t K &= \beta^k \partial_k K - D_i D^i \alpha + \alpha \left(\tilde{A}_{ij} \tilde{A}^{ij} + \frac{1}{3} K^2 \right) + 4 \pi G \alpha ( \rho + \bot T) \\
\partial_t \tilde{\Gamma}^i &= \beta^j \partial_j \tilde{\Gamma}^i - \tilde{\Gamma}^j \partial_j \beta^i+ \frac{2}{3} \tilde{\Gamma}^i \partial_j \beta^j + \tilde{\gamma}^{jk} \partial_j \partial_k \beta^i + \frac{1}{3} \tilde{\gamma}^{ij} \partial_j \partial_k \beta^k \nonumber \\
					&\indent - 2 \tilde{A}^{ij} \partial_j \alpha + 2 \alpha \left(\tilde{\Gamma}_{jk}^i \tilde{A}^{jk} + 6 \tilde{A}^{jk} \partial_k \bar{\phi} -\frac{2}{3} \tilde{\gamma}^{ij} \partial_j K - 8 \pi G e^{4 \bar{\phi}} J^i \right)
\end{align}

\section{Mathematical Formulations of the Eintein-Proca-Dilaton Theory}

There is no exact way to describe this system (based on my search so far), but we can think EP system that interacts with a scalar field, in this case dilaton field $\phi$. An action for EPD is
%Action
\begin{align}
\label{eqn:action:epd}
S=\int d^4 x \sqrt{-g} \left[R - 2 (\nabla \phi)^2 - e^{-2 \alpha_0 \phi} F^2 - 2 \mu_v^2 e^{-2 \alpha_1 \phi}  X^2 \right]
\end{align}
where $\alpha_0$ and $\alpha_1$ are coupling constant. If we consider $\alpha_1=0$, the action looks like
\begin{equation}
S=\int d^4 x \sqrt{-g} \left[R - 2 (\nabla \phi)^2 - e^{-2 \alpha_0 \phi} F^2 - 2 \mu_v^2 X^2 \right]
\end{equation}
Note that there is no coupling between Proca field and dilaton field. This represents Lifshitz BH of EP theory coupled to an non-dynamical scalar field. 

From the action, Eqn.~\ref{eqn:action:epd}, we can derive the equations of motions

For dilaton,
\begin{align}
\label{eqn:eom:dil}
\nabla_a \nabla^a \phi = -\alpha_0 e^{-2 \alpha_0 \phi} F^2 - 2 \mu_v^2 \alpha_1 e^{-2 \alpha_1 \phi} X^2
\end{align}

For Proca,

\begin{align}
\label{eqn:eom:proca}
\nabla_a (e^{-2 \alpha_0} F^{ab}) = 2 \mu_v^2 e^{-2 \alpha_1 \phi} X^b
\end{align}

And Einstein equations
\begin{align}
\label{eqn:eom:EE}
R_{ab} - \frac{1}{2} g_{ab} R = 2 \nabla_a \phi \nabla_b \phi - g_{ab} \nabla_a \phi \nabla^a \phi + 2 e^{-2 \alpha_0 \phi} \left(F_{ac} F^c_b - \frac{1}{4} g_{ab} F^2 \right) + 4 \mu_v^2 e^{-2 \alpha_1 \phi} \left(X_a X_b - \frac{1}{2} g_{ab} X^c X_c \right)
\end{align}
where we can define the energy-momentum tensor 
\begin{align}
T_{ab} = 2 \nabla_a \phi \nabla_b \phi - g_{ab} \nabla_a \phi \nabla^a \phi + 2 e^{-2 \alpha_0 \phi} \left(F_{ac} F^c_b - \frac{1}{4} g_{ab} F^2 \right) + 4 \mu_v^2 e^{-2 \alpha_1 \phi} \left(X_a X_b - \frac{1}{2} g_{ab} X^c X_c \right)
\end{align}

Here we use same 3+1 decomposition and BSSN variable as previous sections, and split Proca field also same as previous section. However, here we evolve electric and magnetic field instead of electric and Proca field. 

From dilaton equation, we define $\Pi \equiv -n^a \nabla_a \phi$ and decomposing into BSSN variables gives
\begin{align}
\partial_t \phi &= \beta^i \partial_i \phi - \alpha \Pi \\
\partial_t \Pi &= \beta^i \partial_i \Pi - \alpha \bar{\chi} \left[\tilde{\gamma}^{ij} \partial_i \partial_j \phi + \tilde{\gamma}^{ij} (\partial_i \ln \alpha) \partial_j \phi - \tilde{\Gamma}^i \partial_i \phi - \frac{1}{2} \tilde{\gamma}^{ij} \partial_i \phi \partial_j \ln \bar{\chi} \right] \nonumber \\
& + \alpha K \Pi - 2 \alpha \alpha_0 e^{-2 \alpha_0 \phi} [(\bot B)^2 - (\bot E)^2] - 2 \mu_v^2 \alpha \alpha_1 (\chi^i \chi_i - \chi_\varphi^2)
\end{align}

From Proca equations, we use constraint damping terms $\Phi$ and $\Psi$ for divergence cleaning. Further, we evolve $\bot E$ and $\bot B$ so we also need to consider $\nabla_a ( \ast F^{ab}) =0 $. Then decompose these sets of equations into BSSN variable
\begin{align}
\partial_t \Psi &= \beta^i \partial_i \Psi - \alpha \left(\partial_i \bot E^i - \frac{3}{2 \bar{\chi}} \bot E^i \partial_i \bar{\chi} \right) + 2 \alpha \alpha_0 \bot E^j \partial_j \phi - 2 \alpha \mu_v^2 e^{-2(\alpha_1 - \alpha_0) \phi} \chi_\varphi - \alpha \eta_1 \Psi \\
\partial_t \Phi &= \beta^i \partial_i \Phi + \alpha\left(\partial_i \bot B^i - \frac{3}{2 \bar{\chi}} \bot B^i \partial_i \bar{\chi} \right)- \alpha \eta_2 \Phi \\
\partial_t \bot E^i &= \beta^j \partial_j \bot E^i - \bot E^j \partial_j \beta^i - \sqrt{\bar{\chi}} \epsilon^{ijk} \left[ \alpha ( \bot B^l \partial_j \tilde{\gamma}_{kl}  + \tilde{\gamma}_{kl} \partial_j \bot B^l) + \tilde{\gamma}_{kl} \bot B^l \left( \partial_j \alpha - \alpha \frac{\partial_b \bar{\chi}}{\bar{\chi}} \right) \right]\nonumber \\
&+ \alpha K \bot E^i  - \alpha \chi \tilde{\gamma}^{ij} \partial_j \Psi  - 2 \alpha \alpha_0 ( \Pi \bot E^i - \epsilon^{ijk} \sqrt{\bar{\chi}} \tilde{\gamma}_{kl} \bot B^l \partial_j \phi) \nonumber \\
& - \alpha e^{2 \alpha_0 \phi} (e^{-2 \alpha_1 \phi} \chi^i + \epsilon^{ijk} \sqrt{\bar{\chi}} \tilde{\gamma}_{kl} \bot E^l \partial_j K \bot E_k ) \\
\partial_t \bot B^i &= \beta^j \partial_j \bot B^i - \bot B^j \partial_j \beta^i - \sqrt{\bar{\chi}} \epsilon^{ijk} \left[ \alpha ( \bot E^l \partial_j \tilde{\gamma}_{kl}  + \tilde{\gamma}_{kl} \partial_j \bot E^l) + \tilde{\gamma}_{kl} \bot E^l \left( \partial_j \alpha - \alpha \frac{\partial_b \bar{\chi}}{\bar{\chi}} \right) \right]\nonumber \\ 
&+\alpha K \bot B^i + \alpha \bar{\chi} \tilde{\gamma}^{ij} \partial_j \Phi
\end{align}

From the Lorenz condition $\nabla^a X_a = 0$ we get :
\begin{align}
\partial_t \chi_i &= \pounds_\beta \chi_i - \frac{\alpha}{\bar{\chi}} \tilde{\gamma}_{ij} E^j - \alpha \partial_i \chi_\varphi - \chi_\varphi \partial_i \alpha \\
\partial_t \chi_\varphi &= \beta^i \partial_i \chi_\varphi + \alpha K \chi_\varphi - \alpha \tilde{\gamma}^{ij} \bar{\chi} \partial_j \chi_i + \alpha \bar{\chi} \chi_i \tilde{\Gamma}^i + \frac{\alpha}{2} \chi_i \tilde{\gamma}^{ij} \partial_j \bar{\chi} - \bar{\chi} \tilde{\gamma}^{ij} \chi_i \partial_j \alpha - \alpha Z \\
\partial_t Z &= \beta^i \partial_i Z + \alpha \partial_i E^i - \frac{3}{2 \bar{\chi}} \alpha E^i \partial_i \chi + \alpha \mu_v^2 \chi_\varphi - \alpha \eta_3 Z
\end{align}
where Z is the constraint damping term for the Proca fields.


From Einstein tensor, we have evolution equations for $\tilde{\gamma}_{ij}$, $\tilde{\Gamma}^i$, $\tilde{A}_{ij}$, $\bar{\chi}$, and $K$
\begin{align}
\partial_t \tilde{\gamma}_{ij} &= \beta^k \partial_k \tilde{\gamma}_{ij} + \tilde{\gamma}_{kj} \partial_i \beta^k -\frac{2}{3} \tilde{\gamma}_{ij} \partial_k \beta^k - 2 \alpha \tilde{A}_{ij} \\
\partial_t \bar{\chi} &= \frac{2}{3} \bar{\chi} ( \alpha K - \partial_k \beta^k) + \beta^k \partial_k \bar{\chi} \\
\partial_t \tilde{A}_{ij} &= \beta^k \partial_k \tilde{A}_{ij} + \tilde{A}_{kj} \partial_j \beta^k -\frac{2}{3} \tilde{A}_{ij} \partial_k \beta^k + e^{-4 \bar{\phi}} \left[ - D_i D_j \alpha + \alpha ^{(3)}R_{ij} - 8 \pi \alpha \bot T_{ij}\right]^{TF} \nonumber \\
				&\indent + \alpha \left( K \tilde{A}_{ij} - 2 \tilde{A}_{ik} \tilde{A}_{j}^k \right) \\
\partial_t K &= \beta^k \partial_k K - D_i D^i \alpha + \alpha \left(\tilde{A}_{ij} \tilde{A}^{ij} + \frac{1}{3} K^2 \right) + 4 \pi  \alpha ( \rho + \bot T) \\
\partial_t \tilde{\Gamma}^i &= \beta^j \partial_j \tilde{\Gamma}^i - \tilde{\Gamma}^j \partial_j \beta^i+ \frac{2}{3} \tilde{\Gamma}^i \partial_j \beta^j + \tilde{\gamma}^{jk} \partial_j \partial_k \beta^i + \frac{1}{3} \tilde{\gamma}^{ij} \partial_j \partial_k \beta^k \nonumber \\
					&\indent - 2 \tilde{A}^{ij} \partial_j \alpha + 2 \alpha \left(\tilde{\Gamma}_{jk}^i \tilde{A}^{jk} -\frac{3}{2 \bar{\chi}} \tilde{A}^{jk} \partial_k \bar{\phi} -\frac{2}{3} \tilde{\gamma}^{ij} \partial_j K - 8 \pi  \frac{J^i}{\bar{\chi}} \right)
\end{align}
where
\begin{align}
\rho &= n^a n^b T_{ab} \nonumber \\
&= \Pi^2 + D_a \phi D^a \phi + e^{-2 \alpha_0 \phi} [ (\bot E)^2 + (\bot B)^2] + 2 \mu_v^2 e^{-2 \alpha_1 \phi} ( \chi^i \chi_i + \chi^2_{\varphi})\\
J^i &= - h^i_a T_{ab} n^b \nonumber \\
&= 2 \Pi D^i \phi + 2 e^{-2 \alpha_0 \phi} \epsilon^{ijk} \bot E_j \bot B_k - 4 \mu_v^2 e^{-2 \alpha_1 \phi} \chi^i \chi_{\varphi} \\
\bot T_{ij} &= h_i^a h_j^b T_{ab} \nonumber \\ 
&= D_i \phi D_j \phi - h_{ij} (D_k \phi D^k \phi - \Pi) + 2 e^{-2 \alpha_0 \phi} \left[\bot F_{ik} + \bot F^k_j + \bot E_i \bot E_j - \frac{1}{2} h_{ij} \{(\bot B)^2 + (\bot E)^2\}\right] \nonumber \\
&+4 \mu_v^2 e^{-2 \alpha_1 \phi} \left[\chi_i \chi_j - \frac{1}{2} (\chi^2 - \chi_\varphi^2) \right]
\end{align}

\section{Initial data for EP and EPD theory}

\subsection{Herdeiro-Radu-Runarsson Solution}
There are several solutions for EP system. Here we use Herdeiro-Radu-Runarsson (HRR) solution to describe Kerr black hole with Proca hair (1603.02687). This works are originally came from Kerr BH with scalar hair (1403.2757). HRR metric is 
\begin{equation}
ds^2 = - e^{2 F_0} N dt^2 + e^{2 F_1} \left(\frac{dr^2}{N} + r^2 d \theta^2 \right) + e^{2 F_2} r^2 \sin^2 \theta ( d \varphi - W dt)^2
\end{equation}
where $W$ and $F_i$, $i=0,1,2$ are function of $r$ and $\theta$ that are asymptotically flat (i.e. it vanishes as $r \rightarrow \infty$) and where $N = 1 - r/r_h$ for event horizon radius $r_h$. Since metric is singular at $r=r_h$, each $F_i$ should satisfy appropriate regularity conditions at the horizon. Define a coordinate
\begin{align}
x = \sqrt{r^2 - r_h}
\end{align}
and demand
\begin{align}
\partial_x F_i = 0 \,\,\,\,\,\,\,\ \forall i = 0,1,2
\end{align}
For vacuum case,

\begin{align}
e^{2 F_1} &= \left(1+\frac{b}{r}\right)^2 + b(b+r_h) \frac{\cos^2 \theta}{r^2} \\
e^{2 F_2} & = e^{- 2 F_1} \left[ \left( \left(1+\frac{b}{r}\right)^2 + \frac{b (b+r_h)}{r^2} \right)^2 - b (b+r_h) \left (1-\frac{r_h}{r} \right) \frac{\sin^2 \theta}{r^2} \right]\\
F_0 &= - F_2 \\
W &= e^{-2 (F_1 + F_2)} \sqrt{b (b+r_h)}\left(1+\frac{b}{r} \right)\frac{r_h + 2b}{r^3} 
\end{align}
where $b$ is the spherodial prolateness parameter which can be defined by
\begin{align}
b = \frac{a^2}{R_{+}}
\end{align}
and $a$ is angular momentum parameter $a=J/M$ and $R_{+}$ is outer EH such that $R_{+}=M+\sqrt{M^2-a^2}$. For proca field $X_a$
\begin{align}
X_a = e^{i (m \phi - \omega t)} (u V dt + H_1 dr + H_2 d \theta + i H_3 \sin \theta d \phi)
\end{align}
where $m$ is winding number $m \in \mathbb{N}$ and frequency $\omega$. The family of $ \{H_1, H_2, H_3, V \}$ is function of $r$ and $\theta$ with similar regularity condition on the horizon
\begin{align}
\partial_x H_i = 0 \,\,\,\,\,\,\,\ \forall i = 1,2, 3
\end{align}
Since coordinate system is axisymmetric, the following symmetry conditions at the symmetry axis
\begin{align}
H_1 = \partial_\theta H_1 = \partial_\theta H_3 = V = \partial_\theta F = \partial_\theta W = 0 \,\,\,\,\, \textrm{at} \,\,\, \theta = 0 ,\pi
\end{align}
Asymptotic flatness implies that the Proca field must be square integrable and so $V$ and $H_i$ should vanish as $r^{-1}$ or faster as $r \rightarrow \infty$

\subsection{Other cases with EP system}
One of the main reasons about studying EP system is finding superradiance instability. Many people study this subject with Einstein-Klein-Gordon system, Einstein with scalar field, and Einstein-Proca. For testing purpose, we can use one of the idea about EP (from East and Pretorius arxiv.1704.0479).

We can study the evolution of Proca fields on a fixed Kerr spacetime with mass $M$ and dimensionless spin $a$. Here, we use Cartesian Kerr-Schild coordinates
\begin{align}
ds^2 &= -dt^2 + dx^2 + dy^2 + dz^2 +\frac{2 M r^3}{r^4 + a^2 M^2 z^2} \Bigg[ dt + \frac{z}{r} dz \nonumber \\
        &+ \frac{r}{r^2 + a^2 M^2} (x dy + y dy) - \frac{a M}{r^2 + a^2 M^2} (x dy- y dz)^2 \Bigg]
\end{align}
where
\begin{align}
\frac{x^2+y^2}{r^2 + a^2} + \frac{z^2}{r^2} = 1
\end{align}

Using this, we can start some stability tests with small spin number $a$ (Extremal spin case is quite challengable)

\subsection{Binary Black Holes in EP and EPD}
There is no well-known analytic ways for both cases. We consider simple case first. (Also, I realize this can be good example before doing real calculations of EMDA because EP is simpler than EMDA). From EP system, we have Hamiltonian and momentum constraints such that
\begin{align}
R + K^2 - K_{ij} K^{ij} &= 2 [\bot E^i \bot E_i + \bot B^i \bot B_i + \mu_v^2(\chi_\varphi^2 + \chi_i \chi^i)] \\
D_j K^{ij} - D^i K &= 2 (\epsilon_{ijk} \bot E^j \bot B^k + \mu_v^2 \chi_\varphi \chi_i)
\end{align}
Additionally, we also satisfy Gauss constraint to set up relevant initial data such that
\begin{align}
D_i E^i = - \mu_v^2 \chi_\varphi
\end{align}
Here, we employ the York-Lichnerowicz conformal decomposition which defines conformal transformation of 3-metric
\begin{align}
h_{ij} = \psi^4 \hat{\gamma}_{ij}
\end{align}
where $\psi$ is arbitrary conformal scalar. Thus, $h^{ij} = \psi^{-4} \hat{\gamma}^{ij}$. Using this transformation, we have
\begin{align}
\Gamma^i_{\,\,\, jk} = \hat{\Gamma}^i_{\,\,\,jk} + 2 (\delta^i_j \partial_k \ln \psi + \delta^i_k \partial_j \ln \psi - \hat{\gamma}_{jk} \hat{\gamma}^{il} \ln \psi)
\end{align}
where $\hat{\Gamma}^i_{\,\,\,jk}$ related with conformal metric $\hat{\gamma}_{ij}$. For Ricci tensor,
\begin{align}
R_{ij} = \hat{R}_{ij} - 2 (\hat{D_i} \hat{D_j} \ln \psi + \hat{\gamma}_{ij} \hat{\gamma}^{lm} \hat{D}_l \hat{D}_m \ln \psi) + 4 [ (\hat{D}_i \ln \psi)(\hat{D}_j \ln \psi) - \hat{\gamma}_{ij} \hat{\gamma}^{lm}(\hat{D}_l \ln \psi)(\hat{D}_m \ln \psi) ]
\end{align}
where $\hat{D}_i$ is covariant derivative in spatial dimension associated with $\hat{\gamma}$. Using that, Ricci scalar is
\begin{align}
R = \psi^{-4} \hat{R} - 8 \psi^{-5} \hat{D}^2 \psi
\end{align}
where $\hat{D}^2 = \hat{\gamma}^{ij} \hat{D}_i \hat{D}_j$. Extrinsic curvature also can be written as following way
\begin{align}
K_{ij} = A_{ij} + \frac{1}{3} h_{ij} K
\end{align}
where $A_{ij}$ is trace-free part i.e. $h_{ij} A^{ij} = 0$. Choose rescale this into conformal metric such that $A^{ij} = \psi^{-10} \hat{A}^{ij}$ then $D_j A^{ij} = \psi^{-10} \hat{D}_j \hat{A}^{ij}$. Also, $A_{ij} = h_{ik} h_{jl} A^{kl} = \psi^8 \hat{\gamma}_{ik} \hat{\gamma}_{jl} \psi^{-10} \hat{A}^{kl} = \psi^{-2} \hat{A}_{ij}$.
Thus, 
\begin{align}
K_{ij} K^{ij} &= \left(A_{ij} + \frac{1}{3} h_{ij} K\right) \left(A^{ij} + \frac{1}{3} h^{ij} K\right) \nonumber \\
                  &= A_{ij} A^{ij} + \frac{1}{3} K^2 = \psi^{-12} \hat{A_{ij}} \hat{A^{ij}} + \frac{1}{3} K^2		
\end{align}
Also, rescale electric and Proca fields like
\begin{align}
\bot E^i &= \psi^{-6} \bot \hat{E}^i \\
\chi_\varphi &= \psi^{-6} \hat{\chi}_\varphi 
\end{align}
and use the fact that $\bot B_i = \epsilon^{ijk} D_j \chi_k$ then substitutes these into constraint equations
\begin{align}
&\hat{D}^2 \psi -\frac{\psi}{8} \hat{R}  + \frac{1}{8 \psi^7} \hat{A}^{ij} \hat{A}_{ij} - \frac{\psi^5}{12} K^2 + \frac{\mu_v^2}{4 \psi^7} ( \hat{\chi}_\varphi^2 + \psi^8 \hat{\gamma}^{ij} \chi_i \chi_j) + \frac{1}{4\psi^3} [\hat{E}_i \hat{E}^i + \hat{D}^j \chi^i ( \hat{D}_j \chi_i - \hat{D}_i \chi_j) = 0 \\
&\hat{D}^j \hat{A}_{ij} - \frac{2}{3} \psi^6 \hat{D}_i K + 2 \hat{E}^j ( \hat{D}_j \chi_i - \hat{D}_i \chi_j) - 2 \mu_v^2 \hat{\chi}_\varphi \chi_i = 0 \\
& \hat{D}_i \hat{E}^i + \mu_v^2 \hat{\chi}_\varphi = 0
\end{align}
These set of elliptic equations are hard to solve. If we assume several things such as conformally flat ($\hat{\gamma}_{ij} = \delta_{ij}$), time symmetry ($K=0$), no vector potential from Proca field i.e. no magnetic component ($\chi_i=0$), and electric field can be obtained same way as Maxwell\apost s theory ($\hat{E}^i = - \delta^{ij} \partial_j V$) where $V$ is arbitrary scalar function, then we can simplify above equations
\begin{align}
&\hat{D}^2 \psi -\frac{\psi}{8} \hat{R}  + \frac{1}{8 \psi^7} \hat{A}^{ij} \hat{A}_{ij} + \frac{\mu_v^2}{4 \psi^7} \hat{\chi}_\varphi^2  + \frac{1}{4\psi^3}\delta^{ij} \partial_i V \partial_j V = 0 \\
&\hat{D}^j \hat{A}_{ij} = 0 \\
& \hat{D}^2 V - \mu_v^2 \hat{\chi}_\varphi = 0
\end{align}
We can even consider further restrictions on these. Same for EMD and EMDA with different RHS of constraints.


\end{document}
