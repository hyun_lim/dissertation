# Run this to generate the figure for the scaling plot
# not in the make file because not everyone may have
# the python dependencies
# python make_weak_scaling_plot.py

from __future__ import print_function
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from os import path
from scipy import optimize

mpl.rcParams['font.size'] = 18

mydir=path.dirname(path.realpath(__file__))
tab_weak \
    = np.loadtxt(path.join(mydir,
                           'tabulated-scaling-test-weak-data.dat'))
untab_weak \
    = np.loadtxt(path.join(mydir,
                           'untabulated-scaling-test-weak-data.dat'))
ncores = tab_weak[...,0]
meantime_tab = tab_weak[...,-1]
meantime_untab = untab_weak[...,-1]

f = lambda x,a,b: a*np.log(x) + b
popt1,pcov1 = optimize.curve_fit(f,ncores,meantime_untab)
popt2,pcov2 = optimize.curve_fit(f,ncores,meantime_tab)
popt3,pcov3 = optimize.curve_fit(f,np.hstack((ncores,ncores)),
                                 np.hstack((meantime_untab,
                                           meantime_tab)))
x = np.linspace(ncores[0],ncores[-1],100)
y1 = f(x,popt1[0],popt1[1])
y2 = f(x,popt2[0],popt2[1])
y3 = f(x,popt3[0],popt3[1])

plt.plot(ncores,meantime_tab,'bo',ms=8,label="tabulated eos")
plt.plot(ncores,meantime_untab,'ro',ms=8,label="analytic eos")
#plt.plot(x,y1,'r--',lw=2,label="fit to "+r'$\log(n)$'+" scaling")
#plt.plot(x,y2,'b--',lw=2)#,label="fit to "+r'$\log(n)$'+" scaling")
plt.plot(x,y1,'g--',lw=2,label=r'$\log(n)$'+" scaling")
plt.legend(framealpha=1.0,loc='upper left')
plt.xlabel('number of cores')
plt.ylabel('average time per iteration (s)')
plt.savefig('weak-scaling-average-times.pdf',
            bbox_inches='tight',
            transparent=True)
